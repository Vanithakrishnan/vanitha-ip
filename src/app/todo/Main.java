package app.todo;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import com.opencsv.exceptions.CsvException;

public class Main {
	private static SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	public static void main(String[] args) throws ParseException {
		Scanner scanner = new Scanner(System.in);
		String fileName = "Test_Data.csv";
		List<Task> listOfTasks = readTaskDataFromFile(fileName);
		int choice = 0;
		//System.out.println(listOfTasks);
		do {
			System.out.println("Welcome to ToDoLy");
			System.out.println("You have X tasks todo and Y tasks are done!");
			System.out.println("Pick an option:");
			System.out.println("(1) Show Task List(by date or project)");
			System.out.println("(2) Add New Task");
			System.out.println("(3) Edit Task(update,mark as done,remove)");
			System.out.print("(4) Save and Quit\n");
			choice = scanner.nextInt();
			scanner.nextLine();
			switch (choice) {

			case 1:
				if (listOfTasks.size() > 0) {
					if (listOfTasks.size() > 1) {
						System.out.println("Enter your sorting option");
						System.out.println("(1) By Date");
						System.out.println("(2) by Project Name");
						int sortOption = scanner.nextInt();
						if (sortOption == 1) {
							listOfTasks.sort((Task t1, Task t2) -> t1.getDueDate().compareTo(t2.getDueDate()));
						} else if (sortOption == 2) {
							listOfTasks.sort((Task t1, Task t2) -> t1.getProject().compareTo(t2.getProject()));
						}
					}
					System.out.println(listOfTasks);
				} else {
					System.out.println("No tasks to display");
				}
				break;

			case 2:
				System.out.println("Enter project name for this task belong to :");
				String project = scanner.nextLine();
				System.out.println("Enter your task title");
				String title = scanner.nextLine();
				System.out.println("Enter due date (format dd/mm/yyyy)");
				String dueDate = scanner.nextLine();
				Date formattedDueDate = dateFormat.parse(dueDate);
				// initially the status of the task should be active. So we don't want to capture that field value from user.
				String status = "active";
				Task task = new Task(title, formattedDueDate, status, project);
				listOfTasks.add(task);
				break;

			case 3:
				System.out.println("Enter your option\n");
				System.out.println("1.Edit");
				System.out.println("2.Mark as Done");
				System.out.println("3.Remove");
				int subChoice = scanner.nextInt();
				scanner.nextLine();
				System.out.println("Enter project name");
				String project1 = scanner.nextLine().toLowerCase();
				System.out.println("Enter task title");
				String title1 = scanner.nextLine().toLowerCase();

				for (Task oneTask : listOfTasks) {
					if (oneTask.getProject().equals(project1) && oneTask.getTitle().equals(title1)) {
						// Here we are going to do changes based on user input
						if (subChoice == 1) {
							System.out.println("Enter the updated project name ");
							oneTask.setProject(scanner.nextLine());
							System.out.println("Enter the updated task title");
							oneTask.setTitle(scanner.nextLine());
							System.out.println("Enter due date (format dd/mm/yyyy)");
							String editedDueDate = scanner.nextLine();
							Date formattedEditedDueDate = dateFormat.parse(editedDueDate);
							oneTask.setDueDate(formattedEditedDueDate);
							System.out.println("Enter your task status");
							oneTask.setStatus(scanner.nextLine());
						} else if (subChoice == 2) {
							oneTask.setStatus("done");
							System.out.println("Status changed as done");
						} else if (subChoice == 3) {
							listOfTasks.remove(oneTask);
							System.out.println("task " + title1 + " in project " + project1 + " removed successfully");
						} else {
							System.out.println("Sorry invalid input...!");
						}
					}
				}
				break;
			case 4:
				System.out.println("Exiting....");
				break;
			default:
				System.out.println("Please enter a valid option");
				break;
			}
			System.out.println("");
		} while (choice != 4);

		if(listOfTasks.size() > 0) {
			writeTaskListToFile(fileName, listOfTasks);
			scanner.close();
		}
		System.out.println("Done.");
	}

	/**
	 * this method helps to read data from file which we are going to given
	 * 
	 * @param fileName
	 * @return list of tasks
	 */
	private static List<Task> readTaskDataFromFile(String fileName) {
		List<Task> listOfTasks = new CopyOnWriteArrayList<>();
		try  {
			FileReader fileReader = new FileReader(fileName);
			CSVReader csvReader = new CSVReader(fileReader);  
			List<String[]> allData = csvReader.readAll();
			for (String[] attributes : allData) {
				listOfTasks.add(createTask(attributes));
			}
		} catch (IOException | ParseException | CsvException e) {
			e.printStackTrace();
		}
		return listOfTasks;
	}

	/**
	 * this will take array of task parameters and create an instance of Task
	 * 
	 * @param attributes
	 * @return
	 * @throws ParseException
	 */
	private static Task createTask(String[] attributes) throws ParseException {
		String project = attributes[0];
		String title = attributes[1];
		String dueDate = attributes[2];
		Date formattedEditedDueDate = dateFormat.parse(dueDate);
		String status = attributes[3];
		return new Task(title, formattedEditedDueDate, status, project);
	}

	/** this method helps to clear the data from file and write updated data into that file.
	 * @param fileName
	 * @param listOfTasks
	 */
	private static void writeTaskListToFile(String fileName,List<Task> listOfTasks) {
		try {
			// Here we are removing existing content
			FileChannel.open(Paths.get(fileName), StandardOpenOption.WRITE).truncate(0).close();
			String csv = fileName;
			CSVWriter writer = new CSVWriter(new FileWriter(csv));
			List<String[]> data=listOfTasks.stream().map(oneTask->(oneTask.getProject()+","+oneTask.getTitle()+","+dateFormat.format(oneTask.getDueDate())+","+oneTask.getStatus()).split(",")).collect(Collectors.toList());
			writer.writeAll(data);
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}