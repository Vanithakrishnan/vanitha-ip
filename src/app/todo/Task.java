package app.todo;

import java.util.Date;

public class Task {
	private String title;
	private Date dueDate;
	private String status;
	private String project;

	public Task(String title, Date dueDate, String status, String project) {
		super();
		this.title = title.toLowerCase();
		this.dueDate = dueDate;
		this.status = status;
		this.project = project.toLowerCase();
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title.toLowerCase();
	}

	/**
	 * @return the dueDate
	 */
	public Date getDueDate() {
		return dueDate;
	}

	/**
	 * @param dueDate
	 *            the dueDate to set
	 */
	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the project
	 */
	public String getProject() {
		return project;
	}

	/**
	 * @param project
	 *            the project to set
	 */
	public void setProject(String project) {
		this.project = project.toLowerCase();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Task [title=" + title + ", dueDate=" + dueDate + ", status=" + status + ", project=" + project + "]";
	}


}
