# User manual
This is the user manual for the todolist project.

## 1. Main menu
When the user starts the application, the main menu is displayed:

```
>> Welcome to MyToDo!
>> You have x tasks todo and y tasks done.
>> Pick an option:
>> (1) Show Task List (by date or project)
>> (2) Add New Task
>> (3) Edit Task (update, mark as done, remove)
>> (4) Save and Quit
```

If an invalid option is selected (5 or 6, for instance), a message is displayed as well as the main menu (after the user pressed Enter):

```
Please enter a valid option
```

If the user presses:

(1) - a **Show Task List (by date or Project)** is displayed; that menu allows the user to see the tasks in the todo list sorted by date or Project.

(2) - a **Add New Task** is able to add new tasks to the todo list.

(3) - an **Edit Task(update,mark as done,remove)** is displayed; that menu allows the user to update,mark as done, remove an existing task;

(4) - a **Save & Quit** the application saves the current list of tasks and quits.

## 2. Show Task List option
 If the user pressed (1), the show menu is displayed:

 ```
 >> Enter your sorting option:
 >> (1) By Project
 >> (2) By date
 ```

If there is only one Task saved then above option will not be displayed. Instead the only task that is saved will be displayed.


If the user presses:

(1) - a list of tasks sorted by date is displayed;

(2) - a list of tasks sorted by project is displayed.

After displaying the list of tasks and once the user pressed Enter, the application returns to main menu.

## 3. Add New task option
If the user pressed (2), to add a new task to the todo list, the following sequence of actions is displayed:
* the application requests user to insert a Project  Name the task will belong to:

```
>> Enter project name for this task belong to :
````

* and to insert a task title:

```
>> Enter your task title:
```

* finally, requests user to insert a due date:

```
>> Enter due date (format dd/mm/yyyy):
```

Once the 3 fields were inserted, a new task was created and  added to the todo list and all the added task has status active.

## 4. Edit Task option
If the user pressed (3), the edit menu is displayed:

```
>> Enter your option:
>> (1) Edit task details
>> (2) Mark task as done
>> (3) Remove task
```
If the user presses:

(1) - a list of tasks with index is displayed:

The user has to provide the following details

 Project name of the task
```
Enter the Project name
```
And the user must enter the task tiltle.

```
>> Enter the  task title:
```
 The updated Project name will be asked. If you dont want to change the project name provide the same project name.

 Provide updated Project name of the task, if you wish to edit the project name else provide the existing name.
```
Enter the updated project name
```
And the user must enter updated the task title if he wish to change it.

```
>> Enter the  updated task title:
```
The user must enter provide a new due date for the edited task.

```
>> Enter the due date (format dd/mm/yyyy):
```
The user must enter provide a status for the edited task.

```
>> Enter your task status
```
The Task is edited now.

```
Provide the Project name
```
Enter the updated project name
```
And the user must enter the task title.
```
The following Message will be displayed.

```
Status changed as done
```
## 5. Save and quit
If the user pressed (4), the todo list is saved in a file (loaded to the program when the user starts the application next time) and the program stops execution:

```
Thank you for using MyToDo.
```
